<?php

namespace Waffler\Definitions;

use Psr\Http\Message\ResponseInterface;

/**
 * Interface Castable.
 *
 * @author ErickJMenezes <erickmenezes.dev@gmail.com>
 */
interface Castable
{
    public static function cast(ResponseInterface $response): self;
}