<?php

namespace Waffler\Definitions\Attributes;

use Attribute;

/**
 * Class Head.
 *
 * @author  ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package Waffler\Definitions\Attributes
 */
#[Attribute(Attribute::TARGET_METHOD)]
class Head extends AbstractHttpMethod
{
    public static function method(): string
    {
        return 'HEAD';
    }
}
