<?php

namespace Waffler\Definitions\Attributes;

use Attribute;

/**
 * Class Suppress.
 *
 * @author  ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package Waffler\Definitions\Attributes
 */
#[Attribute(Attribute::TARGET_METHOD)]
class Suppress
{
}
