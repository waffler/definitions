<?php

namespace Waffler\Definitions\Attributes;

use Attribute;

/**
 * Class Delete.
 *
 * @author  ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package Waffler\Definitions\Attributes
 */
#[Attribute(Attribute::TARGET_METHOD)]
class Delete extends AbstractHttpMethod
{
    public static function method(): string
    {
        return 'DELETE';
    }
}
