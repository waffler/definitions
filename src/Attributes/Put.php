<?php

namespace Waffler\Definitions\Attributes;

use Attribute;

/**
 * Class Put.
 *
 * @author  ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package Waffler\Definitions\Attributes
 */
#[Attribute(Attribute::TARGET_METHOD)]
class Put extends AbstractHttpMethod
{
    public static function method(): string
    {
        return 'PUT';
    }
}
