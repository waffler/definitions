<?php

namespace Waffler\Definitions\Attributes;

use Attribute;

/**
 * Class QueryParams
 *
 * An key-value array to be used as query strings.
 *
 * @author ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package Waffler\Definitions\Attributes
 */
#[Attribute(Attribute::TARGET_PARAMETER)]
class Query
{
}
