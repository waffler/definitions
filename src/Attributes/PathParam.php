<?php

namespace Waffler\Definitions\Attributes;

use Attribute;

/**
 * Class PathParam.
 *
 * @author  ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package Waffler\Definitions\Attributes
 */
#[Attribute(Attribute::TARGET_PARAMETER)]
class PathParam
{
    public function __construct(
        public string $name
    ) {
    }
}
