<?php

namespace Waffler\Definitions\Attributes;

use Attribute;

/**
 * Class Post.
 *
 * @author  ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package Waffler\Definitions\Attributes
 */
#[Attribute(Attribute::TARGET_METHOD)]
class Post extends AbstractHttpMethod
{
    public static function method(): string
    {
        return 'POST';
    }
}
