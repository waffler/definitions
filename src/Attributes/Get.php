<?php

namespace Waffler\Definitions\Attributes;

use Attribute;

/**
 * Class Get.
 *
 * @author  ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package Waffler\Definitions\Attributes
 */
#[Attribute(Attribute::TARGET_METHOD)]
class Get extends AbstractHttpMethod
{
    public static function method(): string
    {
        return 'GET';
    }
}
