<?php

namespace Waffler\Definitions\Attributes;

use Attribute;

/**
 * Class Patch.
 *
 * @author  ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package Waffler\Definitions\Attributes
 */
#[Attribute(Attribute::TARGET_METHOD)]
class Patch extends AbstractHttpMethod
{
    public static function method(): string
    {
        return 'PATCH';
    }
}
