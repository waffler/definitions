<?php

namespace Waffler\Definitions\Attributes;

use Attribute;

/**
 * Class Json.
 *
 * @author  ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package Waffler\Definitions\Attributes
 */
#[Attribute(Attribute::TARGET_PARAMETER)]
class Json
{
}
