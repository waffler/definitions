<?php

namespace Waffler\Definitions\Attributes;

use Attribute;

/**
 * Class QueryParam.
 *
 * @author  ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package Waffler\Definitions\Attributes
 */
#[Attribute(Attribute::TARGET_PARAMETER)]
class QueryParam
{
    public function __construct(
        public string $key
    ) {
    }
}
