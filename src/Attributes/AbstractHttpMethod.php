<?php

namespace Waffler\Definitions\Attributes;

/**
 * @author  ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package Waffler\Definitions\Attributes
 */
abstract class AbstractHttpMethod
{
    public function __construct(
        public string $path
    ) {
    }

    abstract public static function method(): string;
}
