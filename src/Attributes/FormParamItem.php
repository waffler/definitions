<?php

namespace Waffler\Definitions\Attributes;

use Attribute;

/**
 * Class FormParam.
 *
 * @author  ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package Waffler\Definitions\Attributes
 */
#[Attribute(Attribute::TARGET_PARAMETER)]
class FormParamItem
{
    public function __construct(
        public string $key
    ) {
    }
}
