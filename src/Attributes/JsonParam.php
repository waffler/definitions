<?php

namespace Waffler\Definitions\Attributes;

/**
 * Class JsonParam.
 *
 * @author  ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package Waffler\Definitions\Attributes
 */
#[\Attribute(\Attribute::TARGET_PARAMETER)]
class JsonParam
{
    public function __construct(
        public string $key
    ) {
    }
}
