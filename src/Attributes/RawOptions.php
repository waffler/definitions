<?php

namespace Waffler\Definitions\Attributes;

/**
 * Class RawOptions.
 *
 * Options to be merged with GuzzleHTTP Client request options.
 *
 * @author  ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package Waffler\Definitions\Attributes
 */
#[\Attribute(\Attribute::TARGET_PARAMETER)]
class RawOptions
{
}