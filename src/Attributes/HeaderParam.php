<?php

namespace Waffler\Definitions\Attributes;

use Attribute;

/**
 * Class HeaderParam.
 *
 * @author  ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package Waffler\Definitions\Attributes
 */
#[Attribute(Attribute::TARGET_PARAMETER)]
class HeaderParam
{
    public function __construct(
        public string $headerName
    ) {
    }
}
