<?php

namespace Waffler\Definitions\Attributes\Auth;

use Attribute;

/**
 * Class Digest.
 *
 * @author  ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package Waffler\Definitions\Attributes\Auth
 */
#[Attribute(Attribute::TARGET_PARAMETER)]
class Digest
{
}
