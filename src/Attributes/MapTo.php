<?php

namespace Waffler\Definitions\Attributes;

/**
 * Class MapTo.
 *
 * @author  ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package Waffler\Definitions\Attributes
 */
#[\Attribute(\Attribute::TARGET_METHOD)]
class MapTo
{
    public function __construct(
        public string $property
    ) {
    }
}
