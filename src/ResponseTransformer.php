<?php

namespace Waffler\Definitions;

use Psr\Http\Message\ResponseInterface;

/**
 * Interface ResponseTransformer.
 *
 * This interface should specify how response transformers must behave.
 * Response Transformers exists for only one reason, get the ResponseInterface and
 * cast to the expected type.
 *
 * @author   ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package  Waffler\Definitions\Contracts
 */
interface ResponseTransformer
{
    /**
     * This method must transform the response to it respective type.
     *
     * @param \Psr\Http\Message\ResponseInterface  $response
     * @param \Waffler\Definitions\MethodInterface $method
     *
     * @return mixed
     * @throws \TypeError If is not possible to do it.
     * @author ErickJMenezes <erickmenezes.dev@gmail.com>
     */
    public function handle(ResponseInterface $response, MethodInterface $method): mixed;
}