<?php

namespace Waffler\Definitions;

/**
 * Interface MethodInterface.
 *
 * @author   ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package  Waffler\Definitions\Contracts
 * @template RType
 */
interface MethodInterface
{
    /**
     * Determine if the method returns an "auto-mapped" interface.
     *
     * @return false|\ReflectionClass<RType>
     * @author ErickJMenezes <erickmenezes.dev@gmail.com>
     */
    public function returnsAutoMapped(): false|\ReflectionClass;

    /**
     * Determine if the method returns an array of "auto-mapped" objects.
     *
     * @return false|\ReflectionClass<RType>
     * @author ErickJMenezes <erickmenezes.dev@gmail.com>
     */
    public function returnsAutoMappedList(): false|\ReflectionClass;

    /**
     * @return string|class-string<RType>
     * @author ErickJMenezes <erickmenezes.dev@gmail.com>
     */
    public function getReturnType(): string;

    /**
     * Determine if the guzzle http exceptions are suppressed.
     *
     * @return bool
     * @author ErickJMenezes <erickmenezes.dev@gmail.com>
     */
    public function isSuppressed(): bool;

    /**
     * Determine if the response has data to unwrap.
     *
     * @return bool
     * @author ErickJMenezes <erickmenezes.dev@gmail.com>
     */
    public function mustUnwrap(): bool;

    /**
     * Retrieves the wrapper property.
     *
     * @return string
     */
    public function getWrapperProperty(): string;

    /**
     * Retrieves the Attribute if the method has, otherwise returns false.
     *
     * @param class-string<T> $name
     *
     * @return false|array<\ReflectionAttribute<T>>
     * @author   ErickJMenezes <erickmenezes.dev@gmail.com>
     * @template T of object
     */
    public function hasAttribute(string $name): false|array;

    /**
     * Determine if the function is async or not.
     *
     * @return bool
     */
    public function isAsynchronous(): bool;
}