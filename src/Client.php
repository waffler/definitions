<?php

namespace Waffler\Definitions;

/**
 * Interface Client.
 *
 * @author   ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package  Waffler\Definitions\Contracts
 * @template T of object
 */
interface Client
{
    /**
     * Creates a new http client instance using the fully qualified class name of any valid
     * PHP interface.
     *
     * @param class-string<T> $interface         The fully qualified class name of your interface.
     *                                           Example: MyClientInterface::class
     * @param array           $options           [Optional] Here you can define initial guzzle client options.
     *
     * @return T
     * @throws \InvalidArgumentException If the given interface is invalid.
     * @see https://docs.guzzlephp.org/en/stable/quickstart.html
     */
    public static function implements(string $interface, array $options = []): object;

    /**
     * To add a response transformer to the client transformers list.
     *
     * @param \Waffler\Definitions\ResponseTransformer ...$responseTransformer
     */
    public static function addResponseTransformer(ResponseTransformer ...$responseTransformer): void;

    /**
     * To retrieve all registered response transformers.
     *
     * @return array<\Waffler\Definitions\ResponseTransformer>
     * @author ErickJMenezes <erickmenezes.dev@gmail.com>
     */
    public function getResponseTransformers(): array;

    //

    /**
     * Creates a new http client instance using the fully qualified class name of any valid
     * PHP interface.
     *
     * @param class-string<T>     $interfaceName The fully qualified class name of your interface.
     *                                           Example: MyClientInterface::class
     * @param string              $baseUri       [Optional] The base URI of the API you want to consume.
     *                                           Example: https://app.exampledomain.com/api/
     * @param array<string,mixed> $options       [Optional] Here you can define some specific guzzle options.
     *
     * @return T
     * @throws \InvalidArgumentException If the given interface is invalid.
     * @see        \Waffler\Definitions\Client::implements() The definitive method to be used.
     * @deprecated Bad function signature, use Client::implements() instead. This method will be removed in the next
     *             major version.
     */
    #[\JetBrains\PhpStorm\Deprecated("Bad function signature.", "%class%::implements(%parameter0%, ['base_uri' => %parameter1%, ...%parameter2%])")]
    public static function createFromInterface(
        string $interfaceName,
        string $baseUri = '',
        array  $options = []
    ): object;
}